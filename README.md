# Cloud - TP 1
# Installer ansible:
```
 python3 -m pip install --user ansible
```

# Ping avec ansible avec un  [Inventaire](inventory.yml):

```
ansible all -i inventory.yml -m ping -k
```
-k pour saisir le mdp de connexion SSH

# Ping avec ansible avec un  [inventaire](inventory.yml) et un [playbook](playbook.yml):

```
ansible-playbook playbook.yml -i inventory.yml -k
```